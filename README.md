## setup

```
npm install
npm run test
```

## teste online

https://service-div.vercel.app/api/index?num=2

https://service-div.vercel.app/api/index?num=45

https://service-div.vercel.app/api/index?num=400

https://service-div.vercel.app/api/index?num=983

## teste local

```
npx vercel dev
```

http://localhost:3000/api/index?num=2

http://localhost:3000/api/index?num=45

http://localhost:3000/api/index?num=400

http://localhost:3000/api/index?num=983

## decisões

- Usei ava por ser mais simples do que o jest
- Usei o vercel ao invés do express ou outro framework para construção de api rest, porque poderia hosperdar o código facilmente

