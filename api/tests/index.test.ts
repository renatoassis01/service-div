import test from "ava";
import axios from "axios";
import { createServer } from "vercel-node-server";
import listen from "test-listen";

import handler from "../index";

let server: any;
let url: any;

test.beforeEach(async () => {
  server = createServer(handler);
  url = await listen(server);
});

test.afterEach(async () => {
  await server.close();
});

test.serial("function is success", async (t) => {
  const { data } = await axios.get(`${url}?num=45`);
  t.deepEqual(data, { primes: [3, 5], dividers: [1, 3, 5, 9, 15, 45] });
});
