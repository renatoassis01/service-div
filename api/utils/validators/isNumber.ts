import { VercelRequest } from "@vercel/node";

const REGEX_IS_NUMBER = /^-?\d+$/;

export const isNumber = (request: VercelRequest): boolean => {
  const num = request.query?.num as string;
  if (!num) return false;
  return REGEX_IS_NUMBER.test(num);
};
