import test from "ava";
import { isNumber } from "../isNumber";

test("isNumber CASE 1 true", (t) => {
  const dummyRequest = { query: { num: "45" } } as any;
  t.true(isNumber(dummyRequest));
});

test("isNumber CASE 2 false", (t) => {
  const dummyRequest = { query: { num: "aaa" } } as any;
  t.false(isNumber(dummyRequest));
});

test("isNumber CASE 3 false", (t) => {
  const dummyRequest = { query: {} } as any;
  t.false(isNumber(dummyRequest));
});
