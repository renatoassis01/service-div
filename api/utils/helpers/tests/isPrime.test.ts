import test from "ava";
import { isPrime } from "..";

test("isPrime CASE 1 must be false", (t) => {
  t.false(isPrime(1));
});

test("isPrime CASE 2 must be false", (t) => {
  t.false(isPrime(4));
});

test("isPrime CASE 3 must be false", (t) => {
  t.false(isPrime(10));
});

test("isPrime CASE 4 must be false", (t) => {
  t.false(isPrime(45));
});

// CASES TRUE

test("isPrime CASE 1 must be true", (t) => {
  t.true(isPrime(2));
});

test("isPrime CASE 2 must be true", (t) => {
  t.true(isPrime(3));
});

test("isPrime CASE 3 must be true", (t) => {
  t.true(isPrime(5));
});

test("isPrime CASE 4 must be true", (t) => {
  t.true(isPrime(23));
});

test("isPrime CASE 5 must be true", (t) => {
  t.true(isPrime(919));
});

test("isPrime CASE 6 must be true", (t) => {
  t.true(isPrime(983));
});
