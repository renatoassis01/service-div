import test from "ava";
import { getPrimes } from "..";

test("getPrimes must be [2, 3, 5, 23, 919, 983]", (t) => {
  const data = [1, 2, 3, 4, 5, 10, 23, 45, 919, 983];
  const expect = [2, 3, 5, 23, 919, 983];
  const result = getPrimes(data);
  t.deepEqual(result, expect);
});
