import test from "ava";
import { getDivAndPrimes } from "..";

test("getDivAndPrimes muste be return object", (t) => {
  const expect = { primes: [3, 5], dividers: [1, 3, 5, 9, 15, 45] };
  const result = getDivAndPrimes(45);
  t.deepEqual(result, expect);
});
