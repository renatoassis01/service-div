import test from "ava";
import { getDividers } from "..";

test("getDividers must be [1, 3, 5, 9, 15, 45]", (t) => {
  const result = getDividers(45);
  t.deepEqual(result, [1, 3, 5, 9, 15, 45]);
});
