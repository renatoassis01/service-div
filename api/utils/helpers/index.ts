export const isPrime = (num: number, currentNumber = 2): boolean => {
  if (num === 1) return false;
  if (num === 2) return true;
  if (currentNumber < num)
    return num % currentNumber === 0 ? false : isPrime(num, currentNumber + 1);
  return true;
};

export const getDividers = (num: number): number[] => {
  const divs = [1];
  for (let currentNumber = 2; currentNumber <= num; currentNumber++) {
    if (num % currentNumber === 0) divs.push(currentNumber);
  }
  return divs;
};

export const getPrimes = (numbers: number[]): number[] => {
  return numbers.filter((num) => isPrime(num));
};

export const getDivAndPrimes = (
  num: number
): { primes: number[]; dividers: number[] } => {
  const dividers = getDividers(num);
  return {
    primes: getPrimes(dividers),
    dividers,
  };
};
