import type { VercelRequest, VercelResponse } from "@vercel/node";
import { getDivAndPrimes } from "./utils/helpers";
import { isNumber } from "./utils/validators/isNumber";

export default (request: VercelRequest, response: VercelResponse) => {
  if (!isNumber(request)) response.status(400).json({ error: "not a number" });

  const num = +request.query.num;
  response.status(200).json(getDivAndPrimes(num));
};
